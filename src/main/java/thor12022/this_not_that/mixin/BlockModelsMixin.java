package thor12022.this_not_that.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.render.block.BlockModels;
import net.minecraft.client.util.ModelIdentifier;
import net.minecraft.util.Identifier;
import thor12022.this_not_that.NotThatBlock;

@Mixin(BlockModels.class)
public class BlockModelsMixin
{
   // Do our blocks actually need a model? No.
   // But this way we don't get flooded with warnings at startup
   @Inject(method = "getModelId(Lnet/minecraft/block/BlockState;)Lnet/minecraft/client/util/ModelIdentifier;", at = @At("HEAD"), cancellable = true)
   private static void loadModel(BlockState bs, CallbackInfoReturnable<ModelIdentifier> cir)
   {
      if (!(bs.getBlock() instanceof NotThatBlock))
      {
         return;
      }

      ModelIdentifier mid = new ModelIdentifier(new Identifier("minecraft", "air"), 
                                                BlockModels.propertyMapToString(Blocks.AIR.getDefaultState().getEntries()));
       cir.setReturnValue(mid);
   }
}
