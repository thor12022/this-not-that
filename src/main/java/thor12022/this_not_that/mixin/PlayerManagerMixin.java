package thor12022.this_not_that.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;

import thor12022.this_not_that.Events;

@Mixin(PlayerManager.class)
public class PlayerManagerMixin
{
   @SuppressWarnings("static-method")
   @Inject(method = "onPlayerConnect", at = @At("TAIL"))
   private void playerJoin(@SuppressWarnings("unused") final ClientConnection connection, final ServerPlayerEntity player, @SuppressWarnings("unused") final CallbackInfo info)
   {
      Events.PLAYER_JOIN.invoker().playerJoin(player);
   }
}
