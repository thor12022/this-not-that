package thor12022.this_not_that.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;

@Mixin(LootableContainerBlockEntity.class)
public interface LootableContainerBlockEntityMixin
{
   @Invoker("getInvStackList")
   public DefaultedList<ItemStack> getInvStackList();
   
}
