package thor12022.this_not_that.config;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import thor12022.this_not_that.ThisNotThat;

public class Config
{   
   final private static GsonBuilder GSON_BUILDER = new GsonBuilder();
   final private static Gson GSON = GSON_BUILDER.setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
   
   public static class Entry<T>
   {
      @Expose
      @SerializedName("old-name")
      public String oldName;

      @Expose
      @SerializedName("new-name")
      public String newName;
      
      public T old;
   }
   
   @Expose
   int version = 1;
   
   @Expose
   public List<Entry<Block>> blocks = new ArrayList<Entry<Block>>();  
   
   @Expose
   public boolean blockItems = true;
   
   @Expose
   public List<Entry<Item>> items = new ArrayList<Entry<Item>>();  
   
   @Expose 
   public boolean playerInventory = true;
   
   @Expose
   public boolean copyEnchantments = true;
      
   public static Config Load(Path configFile)
   {
      Config config = new Config();
      
      if(!configFile.toFile().exists())
      {
         config.write(configFile);
      }
      else
      {
         boolean error = false;
         try
         {
            config = GSON.fromJson(Files.newBufferedReader(configFile), Config.class);
         }
         catch(JsonSyntaxException | JsonIOException | IOException e)
         {
            ThisNotThat.LOGGER.info("Configuration file issue", e);
            error = true;
         }
         
         if(error)
         {
            try
            {
               ThisNotThat.LOGGER.info("Attempting V0 configuration");
               ConfigV0 configV0 = GSON.fromJson(Files.newBufferedReader(configFile), ConfigV0.class);
               config = configV0.convertToV1();
            }
            catch(JsonSyntaxException | JsonIOException | IOException e)
            {
               ThisNotThat.LOGGER.info("Configuration file (V0) issue", e);
               error = true;
            }
         }
         
         if(error)
         {
            ThisNotThat.LOGGER.warn("Cannot read configuration");
         }
         else
         {
            config.write(configFile);
         }
      }
      
      return config;
   }
   
   public void write(Path configFile)
   {
      try
      {
         FileWriter file = new FileWriter(configFile.toString());
         GSON.toJson(this, file);
         file.close();
      }
      catch(JsonIOException | IOException e)
      {
         ThisNotThat.LOGGER.warn("Cannot write default configuration", e);
         e.printStackTrace();
      }
   }
}
