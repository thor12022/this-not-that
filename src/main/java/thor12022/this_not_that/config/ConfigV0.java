package thor12022.this_not_that.config;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.minecraft.block.Block;

class ConfigV0
{
   private static class ConfigEntry
   {
      @Expose
      @SerializedName("old-block")
      public String oldBlockName;

      @Expose
      @SerializedName("new-block")
      public String newBlockName;
   }
       
   @Expose
   public List<ConfigEntry> blocks = new ArrayList<ConfigEntry>();  
   
   
   Config convertToV1()
   {
      Config configV1 = new Config();
      
      for(ConfigEntry block : blocks)
      {
         Config.Entry<Block> entryV1 = new Config.Entry<Block>();
         entryV1.newName = block.newBlockName;
         entryV1.oldName = block.oldBlockName;
         
         
         configV1.blocks.add(entryV1);
      }
      
      configV1.version = 1;
      
      return configV1;
   }
}
