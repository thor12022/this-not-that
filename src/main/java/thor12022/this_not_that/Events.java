package thor12022.this_not_that;

import org.jetbrains.annotations.Nullable;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.server.network.ServerPlayerEntity;

public class Events {
   @FunctionalInterface
   public interface PlayerJoin {
      @Nullable
      void playerJoin(ServerPlayerEntity player);
   }
   
   public static final Event<PlayerJoin> PLAYER_JOIN = EventFactory.createArrayBacked(PlayerJoin.class, callbacks -> (player) -> {
      for (PlayerJoin callback : callbacks) {
         callback.playerJoin(player);

      }
   });
}
