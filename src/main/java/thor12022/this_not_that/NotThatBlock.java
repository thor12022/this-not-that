package thor12022.this_not_that;

import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.ShapeContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;

public class NotThatBlock extends Block
{
   NotThatBlock()
   {
      super(AbstractBlock.Settings.of(Material.AIR).noCollision().dropsNothing().air());
   }
   
   @Override
   public BlockRenderType getRenderType(BlockState blockState) 
   {
      return BlockRenderType.INVISIBLE;
   }

   @Override
   public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
      return VoxelShapes.empty();
   }
} 