package thor12022.this_not_that;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerChunkEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.chunk.ChunkSection;
import net.minecraft.world.chunk.WorldChunk;
import thor12022.this_not_that.command.Commands;
import thor12022.this_not_that.config.Config;
import thor12022.this_not_that.mixin.LootableContainerBlockEntityMixin;

public class ThisNotThat implements ModInitializer
{
   public static final String MOD_ID = "this_not_that";
   public static final Logger LOGGER = LogManager.getLogger(MOD_ID);
   
   final private Config config = Config.Load(FabricLoader.getInstance().getConfigDir().resolve(MOD_ID + ".json"));
   final private Commands command = new Commands();
   final private Map<Block, BlockState> replacementBlocks = new HashMap<Block, BlockState>();
   final private Map<Item, Item> replacementItems = new HashMap<Item, Item>();
   
   private boolean isLoaded = false;
   
   
   public ThisNotThat()
   {
      ServerChunkEvents.CHUNK_LOAD.register(this::onChunkLoad);
      ServerLifecycleEvents.SERVER_STARTING.register(this::onServerStarting);
      
      if(config.playerInventory)
      {
         Events.PLAYER_JOIN.register(this::onPlayerJoin);
      }
      
      // first create the blocks we'll need from the config
      for (Config.Entry<Block> entry : config.blocks) 
      {  
         entry.old = new NotThatBlock();
      }
      
      // first create the blocks we'll need from the config
      for (Config.Entry<Item> entry : config.items) 
      {  
         entry.old = new NotThatItem();
      }
      
   } 
   
   @Override
   public void onInitialize()
   {
      command.register(MOD_ID);
      
      registerBlocks();
      registerItems();
   }

   private void registerBlocks()
   {
      int count = 0;
      // Then register all the fake blocks to replace
      for (Config.Entry<Block> entry : config.blocks) 
      {  
         if(entry.oldName == null)
         {
            continue;
         }
         try
         {
            Identifier id = new Identifier(entry.oldName);
            if(!Registry.BLOCK.containsId(id))
            {
               Registry.register(Registry.BLOCK, id, entry.old);
               if(config.blockItems)
               {
                  Registry.register(Registry.ITEM, id, new BlockItem(entry.old, new FabricItemSettings()));
               }
               ++count;
            }
            else
            {
               LOGGER.warn("Block to replace still exists: \"" + entry.oldName + "\"");
            }
         }
         catch(InvalidIdentifierException e)
         {
            LOGGER.warn("Invalid block to replace: \"" + entry.oldName + "\"", e);
         }
      }
      LOGGER.info("Registered " + count + " blocks to replace");
   }
   
   private void registerItems()
   {
      int count = 0;
      for(Config.Entry<Item> entry : config.items)
      {
         if(entry.oldName == null)
         {
            continue;
         }
         try
         {
            Identifier id = new Identifier(entry.oldName);
            if(!Registry.ITEM.containsId(id))
            {
               Registry.register(Registry.ITEM, id, entry.old);
               count++;
            }
            else
            {
               LOGGER.warn("Item to replace still exists: \"" + entry.oldName + "\"");
            }
         }
         catch(InvalidIdentifierException e)
         {
            LOGGER.warn("Invalid item to replace: \"" + entry.oldName + "\"", e);
         }
      }
      LOGGER.info("Registered " + count + " items to replace");
   }
   
   private void onServerStarting(@SuppressWarnings("unused") MinecraftServer server)
   {
      if(!isLoaded)
      {
         setupReplacementBlocks();
         setupReplacementItems();
         isLoaded = true;
      }
   }
   
   private void setupReplacementBlocks()
   {
      int count = 0;
      // Need to update the block mapping with the replacement blocks now
      for (Config.Entry<Block> entry : config.blocks) 
      {
         if(entry.oldName == null)
         {
            continue;
         }
         try
         {
            Optional<Block> block = Registry.BLOCK.getOrEmpty(new Identifier(entry.newName));
            if(block.isPresent())
            {
               if(!(block.get() instanceof BlockEntityProvider))
               {
                  replacementBlocks.put(entry.old, block.get().getDefaultState());
                  ++count;
               }
               else
               {
                  LOGGER.warn("Cannot use replacement block: \"" + entry.newName + "\"");
               }
            }
            else
            {
               LOGGER.warn("Invalid replacement block: \"" + entry.newName + "\"");
            }
         }
         catch(InvalidIdentifierException e)
         {
            LOGGER.warn("Invalid replacement block: \"" + entry.newName + "\"", e);
         }
      }
      LOGGER.info("Setup " + count + " replacement blocks");
   }
   
   private void setupReplacementItems()
   {
      int count = 0;
      // Need to update the block mapping with the replacement items now
      for (Config.Entry<Item> entry : config.items) 
      {
         if(entry.oldName == null)
         {
            continue;
         }
         try
         {
            Optional<Item> item = Registry.ITEM.getOrEmpty(new Identifier(entry.newName));
            if(item.isPresent())
            {
               replacementItems.put(entry.old, item.get());
               ++count;
            }
            else
            {
               LOGGER.warn("Invalid replacement item: \"" + entry.newName + "\"");
            }
         }
         catch(InvalidIdentifierException e)
         {
            LOGGER.warn("Invalid replacement item: \"" + entry.newName + "\"", e);
         }
      }
      LOGGER.info("Setup " + count + " replacement items");
   }
   
   
//   private long chunkCheckMicros = 0;
//   private long chunkCount = 0;
   
   private void onChunkLoad(@SuppressWarnings("unused") ServerWorld world, WorldChunk chunk)
   {
//      long time = System.nanoTime();
      
      boolean madeChanges = replaceBlocks(chunk);
      madeChanges |= replaceItems(chunk);
      
      if(madeChanges)
      {
         chunk.markDirty();
      }
      
//      time = System.nanoTime() - time;
//      if(time > 0)
//      {
//         chunkCount++;
//         chunkCheckMicros += time / 1000;
//         LOGGER.info("Chunk Check time: : " + chunkCheckMicros / chunkCount + " μs");
//      }
   }
   
   private void onPlayerJoin(ServerPlayerEntity player)
   {
      replaceInInventory(player.getInventory(), true);

      replaceInInventory(player.getEnderChestInventory(), true);
   }
   
   private boolean replaceBlocks(WorldChunk chunk)
   {
      if(replacementBlocks.isEmpty())
      {
         return false;
      }
      
      boolean madeChanges = false;
      for(ChunkSection section : chunk.getSectionArray())
      {
         if(section == null || section.isEmpty())
         {
            continue;
         }
         for(int x = 0; x < ChunkSection.field_31406; ++x)
         {
            for(int y = 0; y < ChunkSection.field_31406; ++y)
            {
               for(int z = 0; z < ChunkSection.field_31406; ++z)
               {
                  Block thatBlock = section.getBlockState(x,y,z).getBlock();
                  if(thatBlock instanceof NotThatBlock)
                  {
                     BlockState thisBlockState = replacementBlocks.get(thatBlock);
                     if(thisBlockState != null)
                     {
                        section.setBlockState(x, y, z, thisBlockState);
                        madeChanges = true;
                     }
                  }
               }
            }
         }
      }
      return madeChanges;
   }
   
   private boolean replaceItems(WorldChunk chunk)
   {
      if((replacementItems.isEmpty() && !config.blockItems) || (config.blockItems && replacementBlocks.isEmpty()))
      {
         return false;
      }
      
      boolean changed = false;
      for(BlockEntity blockEntity: chunk.getBlockEntities().values()) 
      {
         if(blockEntity instanceof Inventory)
         {
            // We don't want to generate the loot tables if we don't have to, it doesn't go well.
            if(blockEntity instanceof LootableContainerBlockEntity)
            {
               changed |= replaceInLootableInventory((LootableContainerBlockEntity)blockEntity);
            }
            else
            {
               changed |= replaceInInventory((Inventory)blockEntity, false);
            }
         }
      };
      
      return changed;
   }
   
   private boolean replaceInLootableInventory(final LootableContainerBlockEntity lootableInventory)
   {
      boolean change = false;
      final DefaultedList<ItemStack> inventory = ((LootableContainerBlockEntityMixin)lootableInventory).getInvStackList();
      for(int slotIndex = 0; slotIndex < inventory.size(); ++slotIndex)
      {
         final ItemStack stack = replaceStack(inventory.get(slotIndex));
         if(stack != null)
         {
            inventory.set(slotIndex, stack);
            change = true;
         }
      }
      
      return change;
   }
   
   private boolean replaceInInventory(final Inventory inventory, final boolean markDirty)
   {
      boolean change = false;
      for(int slotIndex = 0; slotIndex < inventory.size(); ++slotIndex)
      {
         final ItemStack stack = replaceStack(inventory.getStack(slotIndex));
         if(stack != null)
         {
            if(inventory.isValid(slotIndex, stack))
            {
               inventory.setStack(slotIndex, stack);
               change = true;
            }
         }
      }
      
      if(markDirty && change)
      {
         inventory.markDirty();
      }
      
      return change;
   }
   
   @Nullable
   private ItemStack replaceStack(final ItemStack thatStack)
   {
      ItemStack thisStack = null;
      
      if(!thatStack.isEmpty())
      {
         final Item item = thatStack.getItem();
         if(item instanceof NotThatItem)
         {
            thisStack = new ItemStack(replacementItems.get(item));
         }
         else if(config.blockItems &&  item instanceof BlockItem && ((BlockItem)item).getBlock() instanceof NotThatBlock)
         {
            try
            {
               thisStack = new ItemStack(replacementBlocks.get(((BlockItem)item).getBlock()).getBlock().asItem());
            }
            catch(NullPointerException e)
            {}
         }
         
         if(thisStack != null)
         {
            thisStack.setCount(Math.min(thisStack.getMaxCount(), thatStack.getCount()));
            
            if(thisStack.isDamageable() && thatStack.isDamaged())
            {
               // Proportional damage
               thisStack.setDamage((thatStack.getDamage() / thatStack.getMaxDamage()) * thisStack.getMaxDamage());
            }
            
            if(thatStack.hasCustomName())
            {
               thisStack.setCustomName(thatStack.getName());
            }
            
            if(config.copyEnchantments && thatStack.hasEnchantments() && thisStack.isEnchantable())
            {
               final Map<Enchantment, Integer> enchantments = EnchantmentHelper.get(thatStack);
               for(Entry<Enchantment,Integer> entry : enchantments.entrySet())
               {
                  if(entry.getKey().type.isAcceptableItem(thisStack.getItem()))
                  {
                     thisStack.addEnchantment(entry.getKey(), entry.getValue());
                  }
               }
            }
         }
      }
      return thisStack;
   }
      
}


