package thor12022.this_not_that.command;

import static net.minecraft.server.command.CommandManager.literal;

import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;


public class Commands
{  
   
   final private CommandInterface commands[] = { new PreferredMatchingCommand(),
                                                 new MatchingCommand() };
   
   public void register(final String root)
   {      
      CommandRegistrationCallback.EVENT.register((dispatcher, dedicated) -> {
         for(CommandInterface command : commands)
         {
            dispatcher.register(command.register(literal(root)));
         }
      });
   }
}
