package thor12022.this_not_that.command;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;

import com.google.common.collect.Sets;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tag.BlockTags;
import net.minecraft.tag.ItemTags;
import net.minecraft.tag.TagGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import thor12022.this_not_that.ThisNotThat;
import thor12022.this_not_that.config.Config;

class MatchingInstance
{  
   final private String modNamespace;
   final private String preferredModNamespace;
   final private TypedInstance<Block> blockInstance = new TypedInstance<Block>(BlockTags.getTagGroup(), Registry.BLOCK);
   final private TypedInstance<Item> itemInstance = new TypedInstance<Item>(ItemTags.getTagGroup(), Registry.ITEM);;
   
   MatchingInstance(final String modNamespace, final String preferredModNamespace)
   {
      this.modNamespace = modNamespace;
      this.preferredModNamespace = preferredModNamespace;
   }
   
   int run() 
   {
      itemInstance.process();
      blockInstance.process();
      
      handleBlockItems();
      
      Config config = new Config();

      blockInstance.addConfigEntries(config.blocks);
      itemInstance.addConfigEntries(config.items);
      
      config.write(FabricLoader.getInstance().getConfigDir().resolve(ThisNotThat.MOD_ID + "." + modNamespace + ".json"));
      
      return 0;
   }
   
   private static boolean checkToCullBlockItem(Item item)
   {
      // BlockItems will be generated automatically from the Blocks, expect for BlockEntiies
      return (item instanceof BlockItem) && !(((BlockItem)item).getBlock() instanceof BlockEntityProvider); 
   }

   // Many tags are only relevant when a Block is in Item form, but they're still relevant to the block.
   private void handleBlockItems()
   {  
      for(Entry<Item, Map<Item, Float>> entryFrom : itemInstance.allReplacements.entrySet())
      {
         final Item item = entryFrom.getKey();
         for(Entry<Item, Float> entryTo : entryFrom.getValue().entrySet())
         {
            final Item replacementItem = entryTo.getKey();
            if(item instanceof BlockItem && replacementItem instanceof BlockItem)
            {      
               // it would be best to combine the tags and compare, but without 2-phase processing...
               //    just setting using the lower difference will do
               final Block block = ((BlockItem)item).getBlock();
               final Block rBlock = ((BlockItem)replacementItem).getBlock();
               final Map<Block, Float> replacementBlocks = blockInstance.allReplacements.get(block);
               if(replacementBlocks != null)
               {
                  final Float blockDifference = replacementBlocks.get(rBlock);
                  final Float itemDifference = entryTo.getValue();
                  if(blockDifference == null || ( itemDifference != null && itemDifference < blockDifference))
                  {
                     replacementBlocks.put(rBlock, itemDifference);
                  }
               }
            }
         }
         entryFrom.getValue().entrySet().removeIf((Entry<Item, Float> entry) -> {return checkToCullBlockItem(entry.getKey());});
      }

      itemInstance.allReplacements.entrySet().removeIf((Entry<Item, Map<Item, Float>> entry) -> {return checkToCullBlockItem(entry.getKey());});
   }
   
   
   private class TypedInstance <T> 
   {
      final private TagGroup<T> tags;
      final private Registry<T> registry;
      final Map<T, Map<T, Float>> allReplacements = new HashMap<T, Map<T, Float>>();
       
      TypedInstance(TagGroup<T> tags, final Registry<T> registry)
      {
         this.tags = tags;
         this.registry = registry;
      }

      void process()
      {
         registry.getIds().stream().filter(this::matchNamespace).forEach(this::processThing);
      }
      
      void addConfigEntries(List<Config.Entry<T>> configEntires)
      {
         for( Entry<T, Map<T, Float>> entry : allReplacements.entrySet())
         {            
            Optional<Entry<T,Float>> best = entry.getValue().entrySet().stream().min((Entry<T,Float> l, Entry<T, Float> r) -> 
               {
                  return l.getValue().compareTo(r.getValue());
               });
            if(best.isPresent())
            {
               Config.Entry<T> configEntry = new Config.Entry<T>();
               configEntry.oldName = registry.getId(entry.getKey()).toString();
               configEntry.newName = registry.getId(best.get().getKey()).toString();
               
               configEntires.add(configEntry);
            }
         }
      }      

      private boolean matchNamespace(final Identifier id)
      {
         return id.getNamespace().equals(modNamespace);
      }
      
      private boolean matchPreferredNamespace(final Identifier id)
      {
         return id.getNamespace().equals(preferredModNamespace);
      }
      
      private float tagCompare(final Set<Identifier> lTags, final Set<Identifier> rTags)
      {
         final float difference = Sets.difference(lTags, rTags).size() + Sets.difference(rTags, lTags).size();
         
         return difference / (lTags.size() + difference) * 9 + 1;
      }
      
      private float nameCompare(final Identifier lId, final Identifier rId)
      {
         if(lId.getPath().equals(rId.getPath()))
         {
            return 1f;
         }
         
         final Set<String> rWords = Sets.newHashSet(rId.getPath().split("[-_.:/]") );
         final Set<String> lWords = Sets.newHashSet(lId.getPath().split("[-_.:/]") );
         
         final float difference = Sets.difference(lWords, rWords).size() + Sets.difference(rWords, lWords).size();
         
         return (difference / (lWords.size() + difference)) * 0.5f + 1;
      }
      
      private float combineComparison(final float current, final float comparison)
      {
         if(current == 0)
         {
            return comparison;
         }
         
         return current * comparison;
      }
      
      private void processThing(final Identifier id)
      {
         final Optional<T> thing = registry.getOrEmpty(id);
         if(thing.isPresent() && !(thing.get() instanceof BlockEntityProvider))
         {
            allReplacements.put(thing.get(), new HashMap<T, Float>());
            for(Identifier tagId : tags.getTagsFor(thing.get()))
            {
               for(T replacementThing : tags.getTag(tagId).values())
               { 
                  if(!(replacementThing instanceof BlockEntityProvider))
                  {
                     final Identifier replacementId = registry.getId(replacementThing);
                  
                     if(!matchNamespace(replacementId) &&
                        !allReplacements.get(thing.get()).containsKey(replacementThing))
                     {
                        // TODO: un-duplicate getting of tags
                        float difference = tagCompare(new HashSet<Identifier>(tags.getTagsFor(thing.get())), 
                                                      new HashSet<Identifier>(tags.getTagsFor(replacementThing)));
                        
                        difference = combineComparison(difference, nameCompare(id, replacementId));
                        
                        
                        if(matchPreferredNamespace(replacementId))
                        {
                           difference = combineComparison(difference, 0.75f);
                        }
                        
                        allReplacements.get(thing.get()).put(replacementThing, difference);
                     }
                  }
               }  
            }
         }
      }
   }
   
}
