package thor12022.this_not_that.command;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.fabricmc.loader.api.ModContainer;
import net.minecraft.text.LiteralText;
import net.fabricmc.loader.api.FabricLoader;

class ModIdArgumentType implements ArgumentType<String>
{
   public static ModIdArgumentType modId() 
   {
      return new ModIdArgumentType(false);
   }

   public static ModIdArgumentType optionalModId() 
   {
      return new ModIdArgumentType(true);
   }

   public static <S> String getModId(CommandContext<S> context, String name) 
   {
      return context.getArgument(name, String.class);
   }

   final boolean allowNone;
   
   private ModIdArgumentType(boolean allowNone)
   {
      this.allowNone = allowNone;
   }
   
   @Override
   public String parse(final StringReader reader) throws CommandSyntaxException
   {
      int argBeginning = reader.getCursor();
      if (!reader.canRead()) 
      {
         reader.skip();
      }
      
      while (reader.canRead() && reader.peek() != ' ') 
      {
         reader.skip();
      }
      
      String modId = reader.getString().substring(argBeginning, reader.getCursor());
      
      if(allowNone == true && modId.isEmpty())
      {
         return modId;
      }
      
      boolean match = false;
      for(ModContainer mod : FabricLoader.getInstance().getAllMods())
      {
         if(mod.getMetadata().getId().equals(modId))
         {
            match = true;
            break;
         }
      }
      
      if(!match)
      {
         throw new SimpleCommandExceptionType(new LiteralText("Unknown Mod ID: "+ modId)).createWithContext(reader);
      }
      
      return modId;
   }

   @Override
   public <S> CompletableFuture<Suggestions> listSuggestions(final CommandContext<S> context, final SuggestionsBuilder builder)
   {
      Collection<ModContainer> mods = FabricLoader.getInstance().getAllMods();
      for(ModContainer mod : mods)
      {
         builder.suggest(mod.getMetadata().getId());
      }
      return builder.buildFuture();
   }   
   
}
