package thor12022.this_not_that.command;

import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.server.command.ServerCommandSource;

public class PreferredMatchingCommand implements CommandInterface
{
   final static private LiteralArgumentBuilder<ServerCommandSource> SUB_COMMAND_NAME = literal("generate");
   final static private RequiredArgumentBuilder<ServerCommandSource, String> MOD_ARGUMENT = argument("mod-id", ModIdArgumentType.modId());
   final static private RequiredArgumentBuilder<ServerCommandSource, String> PREFERRED_MOD_ARGUMENT = argument("preferred-mod-id", ModIdArgumentType.optionalModId());
   
   @Override
   public LiteralArgumentBuilder<ServerCommandSource> register(final LiteralArgumentBuilder<ServerCommandSource> commandSoFar)
   {
      return commandSoFar.then(SUB_COMMAND_NAME.then(MOD_ARGUMENT.then(PREFERRED_MOD_ARGUMENT.executes(this))));
   }
   
   @Override
   public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException
   {
      String modNamespace = ModIdArgumentType.getModId(context, MOD_ARGUMENT.getName());
      String preferredModNamespace = ModIdArgumentType.getModId(context, PREFERRED_MOD_ARGUMENT.getName());
      return new MatchingInstance(modNamespace, preferredModNamespace).run();
   }   
}
